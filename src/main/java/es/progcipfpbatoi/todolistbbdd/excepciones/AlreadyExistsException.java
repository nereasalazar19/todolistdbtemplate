package es.progcipfpbatoi.todolistbbdd.excepciones;

import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;

public class AlreadyExistsException extends Exception {
	
	public AlreadyExistsException(Tarea tarea) {
		super("La tarea con código " + tarea + " ya existe");
	}

}
