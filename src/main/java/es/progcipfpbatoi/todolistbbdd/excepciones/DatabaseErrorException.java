package es.progcipfpbatoi.todolistbbdd.excepciones;

public class DatabaseErrorException extends RuntimeException {

    public DatabaseErrorException(String message) {
        super(message);
    }

}

